# J

J is an open-source JavaScript framework for building user interfaces and single-page applications.  

## Syntax & Usage

### Adding to a webpage
```html
<!-- Include J Libraries -->
<script src="/path/to/j.min.js"></script>

<!-- Include custom scripts -->
<script src="/path/to/script.js"></script>
```

### Text
```html
<span>I would like to print variableName right here -> {.variableName.}</span>
```
### Classes
```html
<span class="{.classVariable.}">I have a dynamic class called "classVariable"!</span>
```
### j-if
```html
<div
  j-if="variable == value"
  >
  If the condition is met I will be rendered.
  If the condition is NOT met I will not be rendered.
</div>
```
### j-show
```html
<div
  j-show="variable == value"
  >
  If the condition is met I will be displayed.
  If the condition is NOT met I will not be displayed.
</div>
```