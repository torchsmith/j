// Declare renderer
app = new J();

// Declare initial values for variables on page
app.state = {
    var1: "hey",
    asdkj: "dude",
    var3: "duddette",
    cls: "yes",
    cls2: "bold",
    isVisible: true,
    isVisible2: false,
    step: 0,
    name: "Alfred",
    count: 0
};

component1 = new JComponent("comp", function(){

    var elements = [];
    
    elements.push(`
        <h1>Title</h1>
        <p>We are called {.name.}</p>
    `);

    return elements;
}, app);

// Declare components
app.components = [
   component1
]

// Initialize renderer
app.start();

// Force render after directly changing state (deprecated)
setInterval(()=>{
    app.state.step++;
    app.render();
},1000);

// Method one of value updating
setTimeout(()=>{
    var state = app.state;
    state.var2 = "oink oink";
    state.cls = "no";
    state.cls2 = "italic";
    state.isVisible = false;
    state.isVisible2 = true;
    state.name = "George";
    app.setState(state);
},2500);


// Method two of value updating
// Update number on button press
function addToCounter() {
    app.updateState({ count: app.state.count + 1 });
}