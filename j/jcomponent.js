class JComponent {
    constructor(name, rend, app) {
        this.el = {};
        this.name = name;
        this.rend = rend;
        this.app = app;
    }

    render() {
        this.el.outerHTML = this.rend();
    }
}