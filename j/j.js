class J {

    constructor() {
        this.oldState = {};
        this.state = {};
        this.sv = [];

        this.components = [];
    }

    setState(e) {
        this.state = e;
        this.render();
    }

    updateState(e) {
        this.state = {
            ...this.state,
            ...e
        };
        this.render();
    }

    start() {
        this.oldState = JSON.parse(JSON.stringify(this.state));
        this.loopChildNodes(document.body);
    }

    render() {
        console.groupCollapsed("Render Logs");
        this.sv.forEach((e, ind) => {
            console.log("---- "+ind,e.name,this.state[e.name],this.oldState[e.name]);
            if(this.state[e.name] === this.oldState[e.name]) {
                console.log("breaking");
                return;
            }

            switch(e.type) {
                case "text":
                    e.node.data = this.state[e.name];
                break;
                case "class":
                    e.node.classList.replace(e.node.classList[e.index], this.state[e.name]);
                break;
                case "j-show":
                    if(!eval(e.equation))
                        e.node.style.display = "none";
                    else
                        e.node.style.display = "block";
                break;
                case "j-if":
                    if(!eval(e.equation) && e.node.parentNode !== null) {
                        e.parentNode.removeChild(e.node);
                    }
                    else if(eval(e.equation)) {
                        e.parentNode.insertBefore(e.node, e.parentNode.childNodes[e.index]);
                    }
                break;
            }
        });
        this.oldState = JSON.parse(JSON.stringify(this.state));
        console.groupEnd();
    }

    loopChildNodes(e) {
        e.childNodes.forEach((a) => {
            if (a.nodeName === "#text") {
                this.parseTextNode(a);
            } else {
                this.parseNode(a);
                this.loopChildNodes(a);
            }
        });
    }

    parseNode(e) {
        for(var i = 0; i < this.components.length; i++) {
            if(e.nodeName.toUpperCase() === this.components[i].name.toUpperCase()) {
                this.components[i].el = e;
                this.components[i].render();
                break;
            }
        }

        e.classList.forEach((c, i) => {
            if(c.indexOf("{") === 0 && c.indexOf("}") === c.length-1
            && c.indexOf(".") === 1 && c.indexOf(".", c.length-3) === c.length-2) {
                var variable = c.substring(2, c.length-2);
                this.sv.push({name: variable, index: i, node: e, type: "class"});
                e.classList.replace(c, this.state[variable]);
            }
        });

        for(var i = 0; i < e.attributes.length; i++) {
            switch(e.attributes[i].name) {
                case "j-show":
                    this.sv.push({name: e.attributes[i].nodeValue.split(" ")[0], equation: "this.state."+e.attributes[i].nodeValue, node: e, type: "j-show"});
                    if(!eval("this.state."+e.attributes[i].nodeValue)) {
                        e.style.display = "none";
                    } else {
                        e.style.display = "block";
                    }
                break;
                case "j-if":
                    var a = 0;
                    var child = e;
                    while( (child = child.previousSibling) != null ) 
                        a++;

                    this.sv.push({name: e.attributes[i].nodeValue.split(" ")[0], equation: "this.state."+e.attributes[i].nodeValue, index: a+2, node: e, parentNode: e.parentNode, type: "j-if"});
                    if(!eval("this.state."+e.attributes[i].nodeValue)) {
                        e.parentNode.removeChild(e);
                    }
                break;
            }
        }

        //console.log(e);

        // if(e.className.includes("{") && e.className.includes("}"))
    }

    parseTextNode(e) {
        var first = false;
        var last = false;

        var firstI = -1;
        var lastI = -1;

        var vFirstI = -1;
        var vLastI = -1;

        for (var i = e.data.length-1; i >= 0; i--) {
            if (e.data[i] === "}") {
                if(i > 0)
                    if(e.data[i-1] === ".")
                        last = true;

                vLastI = i-1;
                lastI = i;
            }
            if (last === true && e.data[i] === "{") {
                if(e.data[i+1] === ".")
                    first = true;

                vFirstI = i+1;
                firstI = i;
            }
            if (first && last) {
                var fsub = e.data.substring(0, firstI);
                var msub = e.data.substring(firstI + 1, lastI);
                var lsub = e.data.substring(lastI + 1, e.data.length);
                var ftext = document.createTextNode(fsub);
                var mtext = document.createTextNode(msub);
                var ltext = document.createTextNode(lsub);

                var variable = e.data.substring(vFirstI + 1, vLastI);
                var stateVariable = "this.state." + variable;

                e.parentNode.prepend(ltext);
                e.parentNode.prepend(mtext);

                if (eval(stateVariable) !== undefined) {
                    mtext.data = eval(stateVariable);
                } else {
                    mtext.data = "";
                }
                e.data = fsub;
                i = e.data.length;
                eval("this.sv.push({name:\"" + variable + "\",node:mtext, type: \"text\"})");
                first = false;
                last = false;
            }
        }
        e.parentNode.insertBefore(e, e.parentNode.childNodes[0]);
    }
}